# Notes for December


### Articles And Sites
- Technology
	- A Team dedicated to creating and maintaining Docker Containers - https://blog.linuxserver.io/
	- Rasberry Pi-Hole - https://pi-hole.net/
	- Urbit - https://urbit.org/
	- Laravel Lumen, Lightweight API - https://lumen.laravel.com/
	- Javascript Isset() Alternative - https://stackoverflow.com/questions/2281633/javascript-isset-equivalent
	- Block certain User Agents in NGINX: - https://www.cyberciti.biz/faq/unix-linux-appleosx-bsd-nginx-block-user-agent/
	- Block Users via IPTables and Apache: https://serverfault.com/questions/690870/iptables-block-user-agent
	- Hunt Down Social Media Accounts with Sherlock - https://github.com/sherlock-project/sherlock
	- This X Does Not Exist - https://thisxdoesnotexist.com/
	- Hacker Spaces around you - https://wiki.hackerspaces.org/Hackerspaces
	- Another Firefox Alternative, GNU compliant and built from PaleMoon - http://multixden.blogspot.com/2019/12/arcticfox-27919-release.html
	- AI Dungeon - https://github.com/AIDungeon/AIDungeon
	- Change up and customize your Spotify Client - https://github.com/khanhas/spicetify-cli

- Philosophy and Thought Provoking
	- Expand on the ideology of the Ethos, Pathos, etc in archaic Greek "rhetorical strategies" - https://louisville.edu/writingcenter/for-students-1/handouts-and-resources/handouts-1/logos-ethos-pathos-kairos 
	- Learn about the different types of Yoga and more - https://vedanta.org/what-is-vedanta/
	- Who Narrated Star Wars? - https://scifi.stackexchange.com/questions/224127/who-is-the-narrator-of-star-wars
 
- Privacy & Security
	- How a former UN Ambassador lost her password and sent sensitive info over unclassified systems - https://arstechnica.com/?p=1604835
	- Uber records rides audio for safety - https://www.theverge.com/2019/11/20/20974814/uber-audio-recording-rides-safety-rideshare-lyft
	- Facebook Scraper tool - http://www.kitploit.com/2019/11/ultimate-facebook-scraper-bot-which.html

### Crystal Lang Projects
- IRC Bot+Library - https://gitgud.io/salival/ayanami
- A RealWorld Use of Crystal Lang Onyx Package + Docker - https://github.com/vladfaust/crystalworld 
- Great Intro To Crystal Programming - https://www.youtube.com/playlist?list=PLYRxaDweTODXivZ-BavM0Oq6X-sVydw1B
- Crystal Mastery Site - https://crystalmastery.io/episodes
- Link Checker - https://github.com/lbarasti/twitch-url-checker
- Official Gitter for Crystal Community - https://gitter.im/crystal-lang/crystal
- Redis Client in Crystal - https://github.com/stefanwille/crystal-redis
- HTML Parser and scraper in Crystal - https://github.com/madeindjs/Crystagiri

### Neat Sites
- Funding App for all sorts of online ventures - https://app.codefund.io/

### Videos
- Terrance McKenna Happy Plants - https://www.youtube.com/watch?v=8MG5gFtZ3U8
- Russell Targ on ESP - https://www.youtube.com/watch?v=hBl0cwyn5GY
- Forming Human Connections - https://www.youtube.com/watch?v=hi97EGoLmGE
- Clean up your if statements - https://www.youtube.com/watch?v=ldqDpmMkXgw
- Hunt Down Social Media Accounts with Sherlock - https://www.youtube.com/watch?v=HrqYGTK8-bo
- 

### Misc For Me
https://kingsroadmerch.com/behemoth/
https://www.w3.org/TR/CSS2/generate.html#scope
Crazy Bass playthrough - https://www.youtube.com/watch?v=_YWR4n0VszI
Yeshmin - https://www.youtube.com/watch?v=4YDfN30sD80







