#!/usr/bin/env bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"
[ -f "$HOME/.config/path" ] && source "$HOME/.config/path"
alias php='/Applications/MAMP/bin/php/php7.1.31/bin/php -c "/Library/Application Support/appsolute/MAMP PRO/conf/php7.1.31.ini"'
alias pear='/Applications/MAMP/bin/php/php7.1.31/bin/pear'
alias pecl='/Applications/MAMP/bin/php/php7.1.31/bin/pecl'
